import React from 'react';
import {Text,StyleSheet,View, Button,Image} from 'react-native';

export default class  extends React.Component{
    static navigationOptions ={
        tabBarLabel: 'Events'
    }
    render (){
        return( 
        <View style={styles.card}>
        <View style={{margin: 20}}>
        <Text style={{fontSize: 30,alignItems:'center'}}>
        International Night</Text>
        <Text>Nov 13, 2017</Text>
        <Text>5:00 PM - 8:00 PM</Text>
        <Text>Student Union Ballroom</Text>
        </View>
        </View>
        )
    }
  
}
const styles = StyleSheet.create({
    card:{
        flex:1,
        backgroundColor: 'white',
        margin: 10,
        marginTop: 100,
        marginBottom: 100,
        borderWidth:1,
        borderColor:'lightgrey',
        borderRadius: 8
    },
})