import React from 'react';
import {Text, View, Button,Image} from 'react-native';

export default class Home extends React.Component{
    static navigationOptions ={
        tabBarLabel: 'Home'
    }
    render (){
        return <View style={
                {
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }
        }>
            <Text style={{fontSize: 30}}>
                Welcome To Mane XP
            </Text>
        </View>

    }
}