import React from 'react';
import {Text, View, Button,Image} from 'react-native';

export default class History extends React.Component{
    static navigationOptions ={
        tabBarLabel: 'History'
    }
    render (){
        return <View style={
                {
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }
        }>
            <Text style={{fontSize: 30}}>
                Events History
            </Text>
        </View>

    }
}