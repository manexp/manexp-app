import React from 'react';
import {Text, View, Button,Image} from 'react-native';

export default class Upcoming extends React.Component{
    static navigationOptions ={
        tabBarLabel: 'Upcoming'
    }
    render (){
        return <View style={
                {
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }
        }>
            <Text style={{fontSize: 30}}>
                 Upcoming Events
            </Text>
        </View>

    }
}