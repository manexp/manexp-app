import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import {TabNavigator} from "react-navigation";
import Home from './Screens/Home'
import Events from './Screens/Events'
import Upcoming from './Screens/Upcoming'
import History from './Screens/History'

var MainScreenNavigator = TabNavigator({
    Home: {screen: Home},
    Events: {screen: Events},
    Upcoming: {screen: Upcoming},
    History: {screen: History}
},
{
    tabBarPosition:'bottom',
    swipeEnabled: true,
    tabBarOptions: {
        activeTintColor: 'gold',
        inactiveBackgroundColor: 'green'

    }

}
);

MainScreenNavigator.navigationOptions={
    title: "Mane XP"
};

export default MainScreenNavigator;

